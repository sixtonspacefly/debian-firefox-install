#!/bin/bash

if [[ $EUID -eq 0 ]]; then
  echo "Don't run this script as root."
  exit 1
fi

echo "[ Downloading the latest version of Firefox ]"
wget -O firefox.tar.bz2 "https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=en-US"

echo "[ Extracting firefox.tar.bz2 ]"
tar -xjf firefox.tar.bz2
sudo mv firefox /opt

echo "[ Creating desktop entry ]"
sudo touch /usr/share/applications/firefox-stable.desktop
sudo bash -c 'cat << EOF > /usr/share/applications/firefox-stable.desktop
[Desktop Entry]
Name=Firefox
Comment=Web Browser
GenericName=Web Browser
X-GNOME-FullName=Firefox Web Browser
Exec=/opt/firefox/firefox %u
Terminal=false
X-MultipleArgs=false
Type=Application
Icon=/opt/firefox/browser/chrome/icons/default/default128.png
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/vnd.mozilla.xul+xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;
StartupWMClass=Firefox
StartupNotify=true
EOF'

echo "[ Creating entry in bin for launching ]"
sudo ln -s /opt/firefox/firefox /usr/local/bin/

echo "[ Setting as chosen alternative ]"
sudo update-alternatives --install /usr/bin/x-www-browser x-www-browser /opt/firefox/firefox 200
sudo update-alternatives --set x-www-browser /opt/firefox/firefox
